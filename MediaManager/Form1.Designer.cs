﻿namespace MediaManager
{
    partial class MovieManagerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.movieList = new System.Windows.Forms.ListView();
			this.namesColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.widthColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.heightColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.channelsColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.pathsColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.processBtn = new System.Windows.Forms.Button();
			this.smallMoviesBtn = new System.Windows.Forms.Button();
			this.rootDirBtn = new System.Windows.Forms.Button();
			this.movieCount = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.dupesBtn = new System.Windows.Forms.Button();
			this.deleteSelectedBtn = new System.Windows.Forms.Button();
			this.verifyBtn = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// movieList
			// 
			this.movieList.CheckBoxes = true;
			this.movieList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.namesColumn,
            this.widthColumn,
            this.heightColumn,
            this.channelsColumn,
            this.pathsColumn});
			this.movieList.FullRowSelect = true;
			this.movieList.GridLines = true;
			this.movieList.Location = new System.Drawing.Point(25, 41);
			this.movieList.Name = "movieList";
			this.movieList.ShowItemToolTips = true;
			this.movieList.Size = new System.Drawing.Size(843, 316);
			this.movieList.TabIndex = 0;
			this.movieList.UseCompatibleStateImageBehavior = false;
			this.movieList.View = System.Windows.Forms.View.Details;
			// 
			// namesColumn
			// 
			this.namesColumn.Text = "Name";
			this.namesColumn.Width = 226;
			// 
			// widthColumn
			// 
			this.widthColumn.Text = "Width";
			this.widthColumn.Width = 97;
			// 
			// heightColumn
			// 
			this.heightColumn.Text = "Height";
			this.heightColumn.Width = 114;
			// 
			// channelsColumn
			// 
			this.channelsColumn.Text = "Channels";
			this.channelsColumn.Width = 77;
			// 
			// pathsColumn
			// 
			this.pathsColumn.Text = "Path";
			this.pathsColumn.Width = 318;
			// 
			// processBtn
			// 
			this.processBtn.Location = new System.Drawing.Point(760, 363);
			this.processBtn.Name = "processBtn";
			this.processBtn.Size = new System.Drawing.Size(108, 23);
			this.processBtn.TabIndex = 1;
			this.processBtn.Text = "Save Movie Info";
			this.processBtn.UseVisualStyleBackColor = true;
			this.processBtn.Click += new System.EventHandler(this.processBtn_Click);
			// 
			// smallMoviesBtn
			// 
			this.smallMoviesBtn.Location = new System.Drawing.Point(25, 362);
			this.smallMoviesBtn.Name = "smallMoviesBtn";
			this.smallMoviesBtn.Size = new System.Drawing.Size(117, 23);
			this.smallMoviesBtn.TabIndex = 2;
			this.smallMoviesBtn.Text = "Show Small Movies";
			this.smallMoviesBtn.UseVisualStyleBackColor = true;
			this.smallMoviesBtn.Click += new System.EventHandler(this.smallMoviesBtn_Click);
			// 
			// rootDirBtn
			// 
			this.rootDirBtn.Location = new System.Drawing.Point(200, 13);
			this.rootDirBtn.Name = "rootDirBtn";
			this.rootDirBtn.Size = new System.Drawing.Size(89, 23);
			this.rootDirBtn.TabIndex = 3;
			this.rootDirBtn.Text = "Set Root Dir";
			this.rootDirBtn.UseVisualStyleBackColor = true;
			this.rootDirBtn.Click += new System.EventHandler(this.rootDirBtn_Click);
			// 
			// movieCount
			// 
			this.movieCount.Enabled = false;
			this.movieCount.Location = new System.Drawing.Point(94, 15);
			this.movieCount.Name = "movieCount";
			this.movieCount.Size = new System.Drawing.Size(100, 20);
			this.movieCount.TabIndex = 4;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(22, 18);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(67, 13);
			this.label1.TabIndex = 5;
			this.label1.Text = "Movie Count";
			// 
			// dupesBtn
			// 
			this.dupesBtn.Location = new System.Drawing.Point(148, 363);
			this.dupesBtn.Name = "dupesBtn";
			this.dupesBtn.Size = new System.Drawing.Size(117, 23);
			this.dupesBtn.TabIndex = 6;
			this.dupesBtn.Text = "Show Dupe Movies";
			this.dupesBtn.UseVisualStyleBackColor = true;
			this.dupesBtn.Click += new System.EventHandler(this.dupesBtn_Click);
			// 
			// deleteSelectedBtn
			// 
			this.deleteSelectedBtn.BackColor = System.Drawing.SystemColors.ButtonShadow;
			this.deleteSelectedBtn.Location = new System.Drawing.Point(416, 363);
			this.deleteSelectedBtn.Name = "deleteSelectedBtn";
			this.deleteSelectedBtn.Size = new System.Drawing.Size(142, 23);
			this.deleteSelectedBtn.TabIndex = 7;
			this.deleteSelectedBtn.Text = "Delete Selected";
			this.deleteSelectedBtn.UseVisualStyleBackColor = false;
			this.deleteSelectedBtn.Click += new System.EventHandler(this.deleteSelectedBtn_Click);
			// 
			// verifyBtn
			// 
			this.verifyBtn.Location = new System.Drawing.Point(793, 11);
			this.verifyBtn.Name = "verifyBtn";
			this.verifyBtn.Size = new System.Drawing.Size(75, 23);
			this.verifyBtn.TabIndex = 8;
			this.verifyBtn.Text = "Verify";
			this.verifyBtn.UseVisualStyleBackColor = true;
			this.verifyBtn.Click += new System.EventHandler(this.verifyBtn_Click);
			// 
			// MovieManagerForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(880, 440);
			this.Controls.Add(this.verifyBtn);
			this.Controls.Add(this.deleteSelectedBtn);
			this.Controls.Add(this.dupesBtn);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.movieCount);
			this.Controls.Add(this.rootDirBtn);
			this.Controls.Add(this.smallMoviesBtn);
			this.Controls.Add(this.processBtn);
			this.Controls.Add(this.movieList);
			this.Name = "MovieManagerForm";
			this.Text = "Movie Manager";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView movieList;
        private System.Windows.Forms.ColumnHeader namesColumn;
        private System.Windows.Forms.ColumnHeader widthColumn;
        private System.Windows.Forms.ColumnHeader heightColumn;
        private System.Windows.Forms.ColumnHeader pathsColumn;
        private System.Windows.Forms.Button processBtn;
        private System.Windows.Forms.Button smallMoviesBtn;
        private System.Windows.Forms.Button rootDirBtn;
        private System.Windows.Forms.TextBox movieCount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button dupesBtn;
		private System.Windows.Forms.ColumnHeader channelsColumn;
		private System.Windows.Forms.Button deleteSelectedBtn;
		private System.Windows.Forms.Button verifyBtn;
	}
}

