﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Xml.Linq;
using System.Linq;
using MediaInfoNET;
using System.Diagnostics;

namespace MediaManager
{
    public partial class MovieManagerForm : Form
    {
        // this is for testing
        private string _rootPath = @"E:\Torrents\scooters";
        // private string _rootPath = string.Empty;
        private string[] _allMovies;
        private IList<Movie> _movieObjects = new List<Movie>();
        public MovieManagerForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (Directory.Exists(_rootPath))
            {
                LoadMovies();
                ProcessMovies();
            }
        }

        private void smallMoviesBtn_Click(object sender, EventArgs e)
        {
            GetSmallMovies();
        }

        private void dupesBtn_Click(object sender, EventArgs e)
        {
            FindDuplicateMovies();
        }

        private void processBtn_Click(object sender, EventArgs e)
        {
            SaveList();
        }

        private void deleteSelectedBtn_Click(object sender, EventArgs e)
        {
            DeleteSelectedMovies();
        }

        private void rootDirBtn_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog movieDirectory = new FolderBrowserDialog();
            movieDirectory.SelectedPath = _rootPath;
            movieDirectory.ShowDialog();
            _rootPath = movieDirectory.SelectedPath;
            LoadMovies();
            ProcessMovies();
        }

        private void verifyBtn_Click(object sender, EventArgs e)
        {
            BatchFileVerify();
        }

        private void LoadMovies()
        {
            _allMovies = Directory.EnumerateFiles(_rootPath, "*.*", SearchOption.AllDirectories)
                .Where(s => 
                s.EndsWith(".mvk", StringComparison.OrdinalIgnoreCase) ||
                s.EndsWith(".mp4", StringComparison.OrdinalIgnoreCase) ||
                s.EndsWith(".avi", StringComparison.OrdinalIgnoreCase)
                ).ToArray();
        }

        private void ProcessMovies()
        {
            // clearing the list of old movie objects
            _movieObjects.Clear();
            foreach (string file in _allMovies)
            {
                MediaInfo mi = new MediaInfo();
                mi.Open(file);
                // some movies just break this for some reason, rudimentary try catch here
                int movieWidth;
                int.TryParse(mi.Get_(StreamKind.Visual, 0, "Width"), out movieWidth);
                int movieHeight;
                int.TryParse(mi.Get_(StreamKind.Visual, 0, "Height"), out movieHeight);
                float channels;
                float.TryParse(mi.Get_(StreamKind.Audio, 0, "Channels"), out channels);
                mi.Close();
                Movie currentMovie = new Movie();
                currentMovie.MoviePath = file;
                currentMovie.MovieName = CleanName(Path.GetFileNameWithoutExtension(file));
                currentMovie.MovieWidth = movieWidth;
                currentMovie.MovieHeight = movieHeight;
                currentMovie.Channels = channels;
                _movieObjects.Add(currentMovie);
            }
            DisplayList(_movieObjects);
        }

        private void DisplayList(IList<Movie> MoviesToShow)
        {
            movieList.Items.Clear();
            foreach (Movie movieToShow in MoviesToShow)
            {
                ListViewItem movieItem = new ListViewItem(movieToShow.MovieName);
                movieItem.SubItems.Add(movieToShow.MovieWidth.ToString());
                movieItem.SubItems.Add(movieToShow.MovieHeight.ToString());
                movieItem.SubItems.Add(movieToShow.Channels.ToString());
                movieItem.SubItems.Add(movieToShow.MoviePath);
                movieList.Items.Add(movieItem);
            }
            movieCount.Text = MoviesToShow.Count.ToString();
        }

        private void GetSmallMovies()
        {
            const int widthHD = 1920;
            const int heightHD = 1080;            
            IList<Movie> smallMovies = new List<Movie>();
            
            foreach(Movie currentMovie in _movieObjects)
            {
                if (currentMovie.MovieWidth < widthHD && currentMovie.MovieHeight < heightHD)
                {
                    smallMovies.Add(currentMovie);
                }
            }
            DisplayList(smallMovies);
        }

        private void FindDuplicateMovies()
        {
            // this will return duplicate movies based on clean names
            IList<Movie> dupes = _movieObjects.Except(_movieObjects.Distinct(new MovieNameEqualityComparer())).ToArray();
            DisplayList(dupes);
        }

        private void DeleteSelectedMovies()
        {
            ListView.CheckedListViewItemCollection selectedMovies = movieList.CheckedItems;
            foreach (ListViewItem selected in selectedMovies)
            {
                MessageBox.Show("Deleting " + selected.SubItems[4].Text);
                System.IO.File.Delete(selected.SubItems[4].Text);
            }
            MessageBox.Show(string.Format("Deleted {0} files and now reloading all files, be patient.", selectedMovies.Count.ToString()));
            // reloading movies
            LoadMovies();
            ProcessMovies();
        }

        private void BatchFileVerify()
        {
            string batPath = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + "\\CheckIntegrity.bat";
            // ListView.CheckedListViewItemCollection selectedMovies = movieList.CheckedItems;
            ListView.ListViewItemCollection selectedMovies = movieList.Items;
            string alertMessage = string.Format("Checking {0} files for integrity", selectedMovies.Count.ToString());
            MessageBox.Show(alertMessage);
            foreach (ListViewItem selected in selectedMovies)
            {
                string filePath = Path.GetFullPath(selected.SubItems[4].Text);
                string logPath = selected.SubItems[0].Text + ".log";
                // TODO replace this
                logPath = Path.Combine(@"C:\Users\Batman\Desktop\Logs", logPath);
                Process process = new Process();
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.FileName = batPath;
                process.StartInfo.Arguments = $@"""{ filePath}"" ""{ logPath}""";
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.UseShellExecute = false;
                // copy pasta
                process.OutputDataReceived += (s, e) => Debug.WriteLine(e.Data);
                process.ErrorDataReceived += (s, e) => Debug.WriteLine($@"Error: {e.Data}");
                // end
                process.Start();
                // copy pasta
                process.EnableRaisingEvents = true;
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();
                // end
                process.WaitForExit();
                selected.Checked = false;
            }
            // check for 0 kb files, aka no errors in transcoding
            // remove these to clean up the logs but we want to keep count of them at least for now
            DeleteEmptyFiles(@"C:\Users\Batman\Desktop\Logs");
            MessageBox.Show(string.Format("Finished c{0}", alertMessage.Substring(1)));
        }

        private void DeleteEmptyFiles(string PathToCheck)
        {
            float totalFiles = Directory.GetFiles(PathToCheck).Length;
            float deletedFiles = 0;
            foreach (string each in Directory.GetFiles(PathToCheck))
            {
                FileInfo fileInfo = new FileInfo(each);
                if (fileInfo.Length == 0)
                {
                    System.IO.File.Delete(each);
                    deletedFiles++;
                }
            }
            MessageBox.Show(string.Format("Deleted {0} empty files out of {1} total checked files.", deletedFiles.ToString(), totalFiles.ToString()));
        }

        private void SaveList()
        {
            ListView.CheckedListViewItemCollection selectedMovies = movieList.CheckedItems;
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
            if (folderBrowser.ShowDialog().ToString() == "OK")
            {
                string outputDir = folderBrowser.SelectedPath;
                XDocument outputDocument = new XDocument();
                XElement root = new XElement("Root", "");
                if (Directory.Exists(outputDir))
                {
                    foreach (ListViewItem selected in selectedMovies)
                    {
                        Movie currentMovie = new Movie();
                        currentMovie.MovieName = selected.SubItems[0].Text;
                        currentMovie.MovieWidth = int.Parse(selected.SubItems[1].Text);
                        currentMovie.MovieHeight = int.Parse(selected.SubItems[2].Text);
                        currentMovie.Channels = float.Parse(selected.SubItems[3].Text);
                        currentMovie.MoviePath = selected.SubItems[4].Text;
                        XElement movieElement = new XElement("Movie", 
                            new XAttribute("Name", currentMovie.MovieName), 
                            new XAttribute ("Path", currentMovie.MoviePath),
                            new XAttribute ("Width", currentMovie.MovieWidth),
                            new XAttribute ("Height", currentMovie.MovieHeight),
                            new XAttribute("Channels", currentMovie.Channels));
                        root.Add(movieElement);
                    }
                    outputDocument.Add(root);
                    outputDocument.Save(Path.Combine(outputDir,"butts.xml"));
                }
            }
        }


        // helper functions

        private string CleanName(string fileName)
        {
            string cleaned = fileName;
            cleaned = cleaned.ToLower();
            if (cleaned.IndexOf("720p") != -1)
            {
                cleaned = cleaned.Substring(0, (cleaned.Length - cleaned.IndexOf("720p")));
            }
            cleaned = cleaned.Replace(".", " ").Replace("-", " ");
            return cleaned;
        }

        private string BuildArgs(ListViewItem item)
        {
            string filePath = Path.GetFullPath(item.SubItems[4].Text);
            string logPath = item.SubItems[0].Text + ".log";
            // TODO replace this
            logPath = Path.Combine( @"C:\Users\Batman\Desktop\Logs",logPath);

            string arg1 = @" -v error -i '";
            string arg2 = @"' -map 0:1 -f null - > '";
            string arg3 = @"' 2>&1";
            string arg = string.Format("{0}{1}{2}{3}{4}",arg1,filePath,arg2,logPath,arg3);

            string fuckingArgs = $@"-v error -i ""{ filePath}"" -map 0:1 -f null - > ""{logPath}"" 2>&1";
            return fuckingArgs;
        }
    }
    class Movie
    {
        public string MoviePath { get; set; }
        public string MovieName { get; set; }
        public int MovieWidth { get; set; }
        public int MovieHeight { get; set; }
        public float Channels { get; set; }
    }

    class MovieNameEqualityComparer : IEqualityComparer<Movie>
    {
        public bool Equals(Movie x, Movie y)
        {
            // Two items are equal if their keys are equal.
            return x.MovieName == y.MovieName;
        }

        public int GetHashCode(Movie obj)
        {
            return obj.MovieName.GetHashCode();
        }
    }
}
